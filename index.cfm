<cfoutput>
<style>
a.navbar-brand{
	padding-left:80px;
	margin-left:10px;
	background: url(assets/img/logo-64.png) no-repeat 10px;
}
.red{color:red !important;}
.green{color:green !important;}
</style>
<cfprocessingdirective pageencoding="utf-8">

<html>
	<head>
		<title>i18nRM</title>
		<link href="assets/css/bootstrap.min.css" rel="stylesheet">

	</head>
	<body>

<cfdbinfo name="qTables"  type="table">
<cfquery name="qLocale" dbtype="query">
	SELECT * FROM qTables
	WHERE
		TABLE_NAME NOT LIKE '%_fr' AND
		TABLE_NAME NOT LIKE '%_ES' AND
		TABLE_NAME <>  'settings'
</cfquery>

<!--- modals --->

<cfinclude template = "inc/new_key_value.cfm"/>
<cfinclude template = "inc/export.cfm"/>

<!--- end modal --->

		<div id="top-nav" class="navbar navbar-inverse navbar-static-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon-toggle"></span>
					</button>
					<a class="navbar-brand" href="##">i18nRM</a>
				</div>
				<div class="collapse navbar-collapse">
		        <ul class="nav navbar-nav navbar-right">
		          <li><a href="##">New Project</a></li>
					 <li><a href="##">New Language</a></li>
					 <li><a data-toggle="modal" data-target="##newKeyValue">New Key/Value</a></li>
		          <li><a data-toggle="modal" data-target="##exportLang">Export</a></li>
		        </ul>
			  </div>
				<!---div class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a class="dropdown-toggle" role="button" data-toggle="dropdown" href="##">
								<i class="glyphicon glyphicon-user"></i> Admin <span class="caret"></span>
							</a>
							<ul id="g-account-menu" class="dropdown-menu" role="menu">
								<li><a href="##">My Profile</a></li>
								<li><a href="##"><i class="glyphicon glyphicon-lock"></i> Logout</a></li>
							</ul>
						</li>
					</ul>
				</div--->
			</div><!--- /container --->
		</div>

		<div class="container">
			<div class="row">
				<!---- get all table --->
				<div class="col-md-2">
					<div id="__lang" style="position:fixed;width: 200px;">
						<div class="list-group">
							<cfloop query="qLocale">
								<a class="list-group-item" href="javascript:getKeys('#qLocale.TABLE_NAME#');">#qLocale.TABLE_NAME#</a>
							</cfloop>
						</div>
					</div>
				</div>

				<!--- get all the keys / show keys here --->
				<div class="col-md-4" id="__key">

				</div>

				<!--- get the value and key - for all languages --->
				<div class="col-md-6">
					<div class="well" id="__value" style="position: fixed;width: 585px; margin: 0, -20px, -20px, -20px;"><div>
				</div>

		</div>

		<script type='text/javascript' src="assets/js/jquery-2.1.3.min.js"></script>
		<script type='text/javascript' src="assets/js/bootstrap.min.js"></script>
		<script>
			function getKeys(tbl) 	{
				$.ajax({
				   type: "GET",
				   url: "ajax.cfc?method=getkeys",
				   data: { table: tbl },
				   success: function (data) {
						data = $.parseJSON(data);
						ul = $("<div class='list-group'>");                    // create a new ul element
						// iterate over the array and build the list
						for (var i = 0, l = data.length; i < l; ++i) {
							req = "";
							if(!data[i].COMPLETE) 	{
								req = "<strong class='red'>!</strong>";
							}
							ul.append("<a class='list-group-item' href=javascript:getValues('"+tbl+"','"+data[i].KEY+"')>"+data[i].KEY + " " + req +" </a>");
						}
						$("##__key").html(ul);
				    }
				});
			}

			function getValues(tbl, _key) 	{

				$.ajax({
				   type: "GET",
				   url: "ajax.cfc?method=getvalues",
				   data: {
						table: tbl,
						key  : _key
					},
				   success: function (data) {
				   		console.log(data);
						data = $.parseJSON(data);
						div = "<form>";

						div = div+'<div class="form-group">';
						div = div+'<label for="edit_key">Key</label>';
						div = div+'<input onchange="saveKey(this)" type="text" class="form-control" data-key="'+ _key + '" data-tbl="'+ tbl + '" id="edit_key" name="edit_key" value="'+ _key +'">';
						div = div+'</div><hr/>'

						// default
						$.each(data, function(lang, value) {

							if(lang=='_') 	{
								div = div+'<div class="form-group">';
								div = div+'<label for="'+ lang+'_'+_key +'">Default (en)</label>';
								div = div+'<textarea rows="5" onchange="saveKeyValue(this)" required class="form-control" data-key="'+ _key + '" data-lang="'+ lang + '" data-tbl="'+ tbl + '" id="'+ lang +'_' + _key + '" name="'+ lang +'_' + _key + '">'+ value[_key] +'</textarea>';
								//div = div+'<input onchange="saveKeyValue(this)" type="text" class="form-control"  value="'+  +'">';
								div = div+'</div>'
							}

						});
						// other languages
						$.each(data, function(lang, value) {
							//console.log(data[lang]);

							if(lang!='_') 	{
								div = div+'<div class="form-group">';
								div = div+'<label for="'+ lang+'_'+_key +'">'+lang +'</label>';
								div = div+'<textarea rows="5" onchange="saveKeyValue(this)" class="form-control" data-key="'+ _key + '" data-lang="'+ lang + '" data-tbl="'+ tbl + '" id="'+ lang +'_' + _key + '" name="'+ lang +'_' + _key + '">'+ value[_key] +'</textarea>';
								//div = div+'<input onchange="saveKeyValue(this)" type="text" class="form-control" data-key="'+ _key + '" data-lang="'+ lang + '" data-tbl="'+ tbl + '" id="'+ lang +'_' + _key + '" name="'+ lang +'_' + _key + '" value="'+ value[_key] +'">';
								div = div+'</div>'
							}

						});
						div = div+'</form>'
						$("##__value").html(div);
				   }
				});

			}

			function saveKeyValue(obj) 	{
				input = $(obj)
				$.ajax({
				   type: "POST",
				   url: "ajax.cfc?method=saveKeyValue",
				   data: {
						table : input.data('tbl'),
						lang  : input.data('lang'),
						key  : input.data('key'),
						value: input.val()
					},
				   success: function (data) {
						// notify the user after saving
						input.addClass("green");
				   },
					error: function(data) {
						input.addClass("red");
			      }
				});
			}

			function saveKey(obj) 	{
				input = $(obj)
				table_val = input.data('tbl');
				$.ajax({
				   type: "POST",
				   url: "ajax.cfc?method=saveKey",
				   data: {
						table : table_val,
						old_key  : input.data('key'),
						new_key: input.val()
					},
				   success: function (data) {
						// do not remove//
						getValues(table_val, input.val())
						// -------------//
						input.addClass("green");
						getKeys(table_val);

				   },
					error: function(data) {
						input.addClass("red");
			      }
				});
			}


		</script>
	</body>
</html>
</cfoutput>
