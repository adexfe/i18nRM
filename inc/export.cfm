<cfoutput>
	<div class="modal fade" id="exportLang" tabindex="-2" role="dialog" aria-labelledby="exportLang">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="exportLangLabel">Export</h4>
				</div>
				<div class="modal-body">
					<form id="exportLangForm">
						<div class="form-group">
							<label for="project">Project</label>
							<cfloop query="qLocale">
								<label class="checkbox-inline">
									<input type="radio" name="project" class="export_project" value="#qLocale.TABLE_NAME#"/> #qLocale.TABLE_NAME#
								</label>
							</cfloop>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button"  onclick="exportLang()" class="btn btn-primary">Export</button>
				</div>
			</div>
		</div>
	</div>

	<script>
		function exportLang() 	{

			var _table_val = $("input.export_project:checked").val();

			$.ajax({
				type: "POST",
				url: "ajax.cfc?method=exportLang",
				data: {
					table  : _table_val
				},
				success: function (data) {
					alert("Files Exported to root dir")
					$('##exportLang').modal('hide')
				}
			});

		}
	</script>
</cfoutput>
