<cfoutput>
	<div class="modal fade" id="newKeyValue" tabindex="-1" role="dialog" aria-labelledby="newKeyValueLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="newKeyValueLabel">New Key/Value</h4>
				</div>
				<div class="modal-body">
					<form id="newKeyValueForm">
						<div class="form-group">
							<label for="project">Project</label>
							<cfloop query="qLocale">
								<label class="checkbox-inline">
									<input type="radio" name="project" class="new_project" value="#qLocale.TABLE_NAME#"/> #qLocale.TABLE_NAME#
								</label>
							</cfloop>
						</div>
						<div class="form-group">
							<label for="new_key">Key</label>
							<input required type="text" class="form-control" id="new_key"/>
						</div>
						<div class="form-group">
							<label for="new_value">Value</label>
							<textarea required class="form-control" id="new_value"></textarea>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button"  onclick="saveNewKeyValue()" class="btn btn-primary">Save</button>
				</div>
			</div>
		</div>
	</div>

	<script>
		function saveNewKeyValue() 	{

			var _table_val = $("input.new_project:checked" ).val();
			var _key = document.getElementById('new_key');
			var _value = document.getElementById('new_value');

			$.ajax({
				type: "POST",
				url: "ajax.cfc?method=saveNewKeyValue",
				data: {
					table : _table_val,
					key   : _key.value,
					value : _value.value
				},
				success: function (data) {
					getKeys(_table_val);
					getValues(_table_val, _key.value)
					_value.value = "";
					_key.value = "";
					$('##newKeyValue').modal('hide')
				}
			});

		}
	</script>
</cfoutput>
