component {

	remote string function getkeys(required string table) returnformat="json"	{

		var local = [];

		query name="qTable" {
			echo("
				SELECT * FROM #arguments.table#
				ORDER BY `Key`
			");
		}
		local.keys = [];
		for(qT in qTable)	{
			// TODO -- automate - the system should know all the languages it wants to interpret
			query name="qFr" 	{
				echo("
					SELECT * FROM #arguments.table#_fr_fr
					WHERE `Key` = '#qT.Key#'
				")
			}
			//abort serialize(local.key);
			arrayAppend(local.keys, {key : qT.Key, complete : keyHasValue(qFr.Value)});
		}

		local.temp =  serialize(local.keys);
		local.temp = replace(local.temp,"'",'"','all'); // the problem is  in the version of railo/lucee 
		return local.temp;
	}

	remote string function getValues(required string table, required string key) returnformat="json"	{

		// get all the langs similar to the table above
		dbinfo name="qLangs" type="table";
		query name="qLangs" dbtype="query" {
			echo("
				SELECT * FROM qLangs
				WHERE TABLE_NAME LIKE '#arguments.table#%'
				ORDER BY TABLE_NAME
			");
		}
		var rt = {};
		var _key = "";
		var lang_name = "";
		for(lang in qLangs)	{
			// get key value
			query name="qKV" {
				echo("
					SELECT * FROM #lang.TABLE_NAME#
					WHERE `Key` = '#arguments.key#'
				");
			}
			_key = replace(qKV.key,'.','_','all');
			if(listlen(arguments.table,'_') eq listlen(lang.TABLE_NAME,'_'))	{
				rt['_'][qKV.key] = qKV.Value;
			}
			else 	{
				lang_name = ReplaceNoCase(lang.TABLE_NAME, arguments.table & "_", "");
				//abort lang_name & "--" & qKV.key & "-" & qKV.Value;
				rt[lang_name][qKV.key] = qKV.Value;
			}
		}

		local.temp =  serialize(rt);

		local.temp = replace(local.temp,"'},'_'",'"},"_"','all') 
		local.temp = replace(local.temp,"':{'",'":{"','all'); 
		local.temp = replace(local.temp,"':'",'":"','all')
		local.temp = replace(local.temp,"'}}",'"}}','all') 
		local.temp = replace(local.temp,"{'",'{"','all');
		return local.temp
	}

	remote void function saveKey(required string table, required string old_key, required string new_key)	{

		//TODO - auto matically insert into other langs
		local.tbls = "";
		local.tbls = ListAppend(local.tbls, arguments.table);
		local.tbls = ListAppend(local.tbls, arguments.table & "_fr_fr");

		transaction {
			loop list="#local.tbls#" item="local.tbl" 	{
				query{
					echo("
						UPDATE #local.tbl# SET `Key`='#arguments.new_key#'
						WHERE `Key`='#arguments.old_key#'
					");
				}
			}
		}
	}

	remote void function saveKeyValue(required string table,required string lang, required string key, required string value){

		if(arguments.lang neq "_")	{
			arguments.table = arguments.table & "_" & arguments.lang;
		}

		query name="qv"{
			echo("SELECT * FROM #arguments.table# WHERE `Key`='#arguments.Key#'")
		}
		local.where = "";
		if(qV.recordcount)	{// update
			local.update_or_insert = "UPDATE #arguments.table# SET ";
			local.where = " WHERE `Key`='#arguments.key#'";
		}
		else 	{ // insert
			local.update_or_insert = "INSERT INTO #arguments.table# SET `Key` = '#arguments.key#', "
		}

		query {
			echo("#local.update_or_insert# `Value` = ");
			queryParam cfsqltype="cf_sql_varchar" value="#arguments.value#";
			echo(local.where);
		}
	}

	remote void function saveNewKeyValue(required string table, required string key, required string value){

		//TODO - auto matically insert into other langs
		local.tbls = "";
		local.tbls = ListAppend(local.tbls, arguments.table);
		local.tbls = ListAppend(local.tbls, arguments.table & "_fr_fr");

		transaction {
			loop list="#local.tbls#" item="local.tbl" 	{
				query{
					echo("
						INSERT INTO #local.tbl# SET
						`Key`='#arguments.key#'
					");
					if(findNoCase('fr_fr', local.tbl)) {
					}
					else 	{
						echo(", `Value`= ");
						queryParam cfsqltype="cf_sql_varchar" value="#arguments.value#";
					}
				}
			}
		}
	}

	remote void function exportLang(required string table)	{

		// get settins in order to know where to upload files
		query name="qS" 	{
			echo("
				SELECT * FROM settings WHERE `Project` = '#arguments.table#'
			")
		}
		//TODO - auto matically insert into other langs
		var tbls = "";
		tbls = ListAppend(local.tbls, arguments.table);
		var tbls = ListAppend(local.tbls, arguments.table & "_fr_fr");
		var js = "";
		loop list="#tbls#" item="tbl" 	{
			query name="keys" {
				echo("
					SELECT * FROM #tbl# ORDER BY `Key`
				");
			}

			// write convert keys into cf struct
			js = "/* -------------------------------------------------" & chr(10);
			js = js & chr(9) & "Resource Bundle" & chr(10);
			js = js & chr(9) & "Generated using https://gitlab.com/adexfe/i18nRM" & chr(10);
			js = js & chr(9) & "Exported on #dateformat(now(),'full')# #timeformat(now(),'full')#" & chr(10) & " ------------------------------------------------ */" & chr(10)  & chr(10);

			if(len(qS.IncludeTemplate) neq 0)	{
				lang = "";
				if(listlast(tbl,'_') eq "FR")	{
					lang = "_fr_fr";
				}
				js = js & chr(9) & 'include template="#qS.IncludeTemplate##lang#.cfm";' & chr(10) & chr(10);
			}


			for(qkey in keys)	{
				js = js & chr(9) & 'request.lang.#qkey.Key# = "' & #qkey.Value# & '";' & chr(10);
			}

			js = "<cfscript>" & chr(10) & js & chr(10) & "</cfscript>";
			// write into file
			file action="write" file="#qS.RootDir##tbl#.cfm" addnewline="No" mode="777" output="#js#" charset="UTF-8";
		}

	}

	private boolean function keyHasValue(required string _value)	{

		var rt = "";
		if(arguments._value eq "")	{
			rt = false;
		}
		else 	{
			rt = true;
		}

		return rt;
	}
}
