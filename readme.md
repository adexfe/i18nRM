i18nRM
==

Generate language file for your language.

Features
-----
- [x] Export language file
- [x] Ajax saving to db
- [ ] Import ```json``` files

Installation
----
1. Create ```i18nrm``` database and datasource
2. Create the language table using the script below:
```sql
CREATE TABLE `project_name` (
  `Key` varchar(255) CHARACTER SET ascii DEFAULT NULL,
  `Value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```
```sql
CREATE TABLE `project_name_fr_fr` (
  `Key` varchar(255) CHARACTER SET ascii DEFAULT NULL,
  `Value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```
> the above will create two tables ```project_name``` and ```project_name_fr_fr```. ```project_name``` will house your default language
3. Create the settings table.
```sql
CREATE TABLE `settings` (
  `SettingsId` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `Project` varchar(200) CHARACTER SET ascii DEFAULT NULL,
  `RootDir` varchar(255) CHARACTER SET ascii DEFAULT NULL,
  `IncludeTemplate` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SettingsId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
```
> the above is use store where to export your finished work
4. fire-up lucee and open http://localhost:8888/i18nrm/ (assuming you have copied this project into your wwwroot)
